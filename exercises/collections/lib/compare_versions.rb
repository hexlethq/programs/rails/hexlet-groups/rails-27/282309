# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  major1, minor1 = version1.split('.')
  major2, minor2 = version2.split('.')

  if major1 > major2 || (major1 == major2 && minor1.to_i > minor2.to_i)
    1
  elsif major1 < major2 || (major1 == major2 && minor1.to_i < minor2.to_i)
    -1
  else
    0
  end
end
# END

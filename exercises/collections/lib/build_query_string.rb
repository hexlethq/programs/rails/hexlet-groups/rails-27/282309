# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(params)
  params.sort.to_a.map { |param| "#{param[0]}=#{param[1]}" }.join("&")
end
# END
# rubocop:enable Style/For

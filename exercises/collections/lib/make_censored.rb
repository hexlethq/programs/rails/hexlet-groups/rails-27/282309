# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  # BEGIN
  result = ''
  words = text.split
  words.each do |word|
    if stop_words.include? word
      result = "#{result} $#%!"
    else
      result = "#{result} #{word}"
    end
  end

  result.strip
  # END
end

# rubocop:enable Style/For

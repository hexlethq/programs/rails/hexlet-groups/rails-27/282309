# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  extend Forwardable
  include Comparable
  attr :url

  def initialize(string)
    @params = {}
    @url = URI string
  end

  def query_params
    result = {}
    arr_par = @url.query.split('&')
    arr_par.each do |param|
      key, value = param.split('=')
      result[:"#{key}"] = value
    end
    @params = result

    result
  end

  def query_param(key, def_value = nil)
    @params.key?(key) ? @params[:"#{key}"] : def_value
  end

  def <=>(other)
    @url.to_s <=> other.url.to_s
  end

  def_delegators :@url, :scheme, :host
end
# END

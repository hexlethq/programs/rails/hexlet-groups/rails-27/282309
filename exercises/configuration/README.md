# Конфигурация

## Ссылки

* [Rollbar.com](https://rollbar.com/)
* [Документация Rollbar для настройки в Rails](https://docs.rollbar.com/docs/rails)

## Задачи

Установите и настройке Rollbar следуя [инструкции](https://docs.rollbar.com/docs/rails)

* Зарегистрируйтесь в [Rollbar](https://rollbar.com/) и создайте новый проект
* Добавьте в *Gemfile* гем - `gem 'rollbar'` и установите его
* Сгенерируйте конфигурацию командой

    ```sh
    bin/rails generate rollbar
    ```

* В *models/config/environment.rb* добавьте перехват ошибок Rollbar

    ```ruby
    require_relative 'rollbar'

    notify = lambda do |e|
      Rollbar.with_config(use_async: false) do
        Rollbar.error(e)
      end
    rescue StandardError
      Rails.logger.error 'Synchronous Rollbar notification failed.  Sending async to preserve info'
      Rollbar.error(e)
    end

    begin
      Rails.application.initialize!
    rescue Exception => e
      notify.call(e)
      raise
    end
    ```

* Экспортируйте переменную с токеном Rollbar

    ```sh
    export ROLLBAR_ACCESS_TOKEN=c632f33fd8bf4f5e981b3a27d4964986
    ```

* Запустите приложение. При попытке открытия главной страницы будет вызываться ошибка. Если Rollbar настроен корректно, то на сайте появится сообщение.

* Когда будете отправлять домашнее задание на проверку, приложите скриншот ошибки в Rollbar. Пример:

![Rollbar error example](assets/rollbar-example.png)

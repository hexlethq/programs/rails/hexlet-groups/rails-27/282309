require "test_helper"

class BulletinsIndexTest < ActionDispatch::IntegrationTest
  test 'can open index' do
    get root_path
    
    assert_response :success
    assert_select 'li', 'Home'
    assert_select 'li', 'Bulletins'
    assert_select 'h1', 'Home'
  end

  test 'open first bulletin' do
    get bulletins_path(bulletins(:bulletin_1))

    assert_response :success
    assert_select "h1", text: 'Bulletins'
    assert_select "li", text: 'first'
  end
end

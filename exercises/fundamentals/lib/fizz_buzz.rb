# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = ''
  while start <= stop
    if (start % 3).zero? && (start % 5).zero?
      result = "#{result} FizzBuzz"
    elsif (start % 5).zero?
      result = "#{result} Buzz"
    elsif (start % 3).zero?
      result = "#{result} Fizz"
    else
      result = "#{result} #{start}"
    end

    start += 1
  end

  result.strip
end
# END

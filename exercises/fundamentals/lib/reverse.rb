# frozen_string_literal: true

# BEGIN
def reverse(string)
  result = []
  arr = string.chars
  arr.each { |c| result.unshift(c) }
  result.join
end
# END

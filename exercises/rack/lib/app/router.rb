require 'rack'

handler = Rack::Handler::Thin

class Router
  def call(env)
    req = Rack::Request.new(env)
    hash_hello = make_hash("Hello, World!")
    hash_about = make_hash("About page")

    if req.path == '/'
      [200, {"Content-Type" => "text/plain"}, ["Hello, World!", hash_hello].join("\n")]
    elsif req.path == '/about'
      [200, {"Content-Type" => "text/plain"}, ["About page", hash_about].join("\n")]
    else
      [404, {"Content-Type" => "text/plain"}, "404 Not Found"]
    end
  end

  def make_hash(string)
    return Digest::SHA256.hexdigest(string)
  end
end
require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    hash = Digest::SHA256.hexdigest(body)
    new_body = [body, hash].join("\n")

    [status, headers, new_body]
  end
end

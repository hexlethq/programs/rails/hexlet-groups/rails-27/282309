# frozen_string_literal: true

class Executor_timer
  def initialize(app)
    @app = app
  end

  def call(env)
    start = Time.now
    status, headers, body = @app.call(env)
    stop = Time.now
    body = "#{body}\n#{(stop - start) * 1_000_000}"
    [status, headers, body]
  end
end
# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_pop_and_push
    @stack.push! 'ruby'
    @stack.push! 'php'
    @stack.push! 'java'

    assert_equal @stack.pop!, 'java'
    assert_equal @stack.pop!, 'php'
    assert_equal @stack.pop!, 'ruby'
    refute @stack.pop!
  end

  def test_empty
    assert @stack.empty?
    @stack.push!('ruby')

    refute @stack.empty?
  end

  def test_to_a
    @stack.push! 'ruby'
    assert_equal @stack.to_a, %w[ruby]
  end

  def test_size
    @stack.push! 'ruby'
    @stack.push! 'php'
    assert_equal @stack.size, 2
  end

  def test_clear
    @stack.push! 'ruby'
    @stack.push! 'php'
    @stack.clear!

    assert_equal @stack.to_a, []
    assert @stack.empty?
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?

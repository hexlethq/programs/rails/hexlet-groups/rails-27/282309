# frozen_string_literal: true

# BEGIN
def get_same_parity(array)
  array.select { |el| array[0].even? ? el.even? : el.odd? }
end
# END

# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  mens = users.filter { |user| user[:gender] == 'male' }
  years = mens.map { |u| u[:birthday][0, 4] }
  result = years.each_with_object({}) do |year, acc|
    acc[year] ||= 0
    acc[year] += 1
  end

  result
end
# END

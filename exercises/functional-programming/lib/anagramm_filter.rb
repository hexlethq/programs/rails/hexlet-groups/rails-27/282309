# frozen_string_literal: true

# BEGIN
def anagramm_filter(string, array)
  result = []
  array.each do |word|
    stack = string.chars

    next if word.length > stack.length

    word.each_char do |char|
      stack.delete_at(stack.index(char)) if stack.include?(char)
    end

    result << word if stack.length.zero?
  end
  result
end
# END
